var path = require('path');
var webpack = require('webpack');
var pkg = require('./package.json');

module.exports = {
  entry: [
    'webpack-hot-middleware/client',
    './src/app'
  ],
  devtool: 'eval',
  devServer: {
    contentBase: './build',
    inline: true
  },
  output: {
    path: path.join(__dirname, './build'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loader: "babel-loader",
      query: {stage: 0}
    }, {
      test: /\.css$/, loader: 'style!css'
    }]
  }
}
